const path = require("path");
const autoprefixer = require("autoprefixer");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
// const WebpackPwaManifest = require("webpack-pwa-manifest");
// const OfflinePlugin = require('offline-plugin');

let htmlMinifyOptions = {
  collapseWhitespace: true,
  html5: true,
  minifyCSS: true,
  removeComments: true,
  removeEmptyAttributes: true
};

const PATH = {
  dist: path.resolve(__dirname, "dist"),
  src: path.resolve(__dirname, "src")
};

module.exports = {
  entry: `${PATH.src}/index.js`,
  output: {
    filename: "js/bundle.[hash].js",
    chunkFilename: "js/[name].bundle.js",
    path: PATH.dist
  },
  module: {
    rules: [
      {
        test: /\.js|.jsx?$/,
        exclude: /(node_modules)/,
        loaders: ["babel-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              config: {
                path: "postcss.config.js"
              },
              plugins: [autoprefixer()]
            }
          },
          "sass-loader"
        ],
        exclude: /(node_modules)/
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              config: {
                path: "postcss.config.js"
              },
              plugins: [autoprefixer()]
            }
          }
        ]
      },
      {
        test: /\.(gif|png|jpg|jpe?g)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]", //[path]
              outputPath: "/img", // /img
              publicPath: "/img"
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "bundle.[hash].css",
      publicPath: "/"
    }),
    new HtmlWebpackPlugin({
      filename: 'consent.html',
      template: `${PATH.src}/assets/consent.html`,
      minify: htmlMinifyOptions
    }),
    new HtmlWebpackPlugin({
      filename: 'regulation.html',
      template: `${PATH.src}/assets/regulation.html`,
      minify: htmlMinifyOptions
    }),
    new HtmlWebpackPlugin({
      template: `${PATH.src}/assets/index.html`,
      minify: htmlMinifyOptions
    }),
    new FaviconsWebpackPlugin(`${PATH.src}/assets/img/logo.png`),
    // new WebpackPwaManifest({
    //   name: "NSK",
    //   short_name: "NSK",
    //   author: "ZVA",
    //   description: "NSK",
    //   display: "standalone",
    //   theme_color: "#002857",
    //   background_color: "#002857",
    //   crossorigin: "use-credentials",
    //   start_url: ".",
    //   icons: [
    //     {
    //       src: path.resolve(__dirname, "..", "src", "assets", "favicon.png"),
    //       sizes: [96, 128, 192, 256, 320, 512, 768]
    //     }
    //   ]
    // }),
    // new OfflinePlugin({
    //   responseStrategy: "cache-first",
    //   autoUpdate: "1000 * 60 * 60 * 24" // последнее число колличество часов
    // })
  ],
  resolve: {
    modules: ["node_modules", path.resolve(__dirname, "..", "src")],
    extensions: [".js", ".json", ".jsx", ".css"]
  },
  optimization: {
    runtimeChunk: {
      name: "manifest"
    },
    splitChunks: {
      chunks: "all",
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          priority: -20,
          chunks: "all"
        }
      }
    },
    namedChunks: true
  }
};
