import "./scss/custom.scss";
import "./assets/img/logo.png";
import "./assets/img/bgnew.jpg";
import "./scss/img/bgnew.jpg";
import "./assets/img/director.jpg";
import "bootstrap";

window.onload = () => {
  const anchors = document.querySelectorAll('a[href*="#"]');

  for (let anchor of anchors) {
    anchor.addEventListener("click", function (e) {
      e.preventDefault();
      const blockID = anchor.getAttribute("href").substr(1);
      document.getElementById(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start"
      });
    });
  }

  if (window.location.hash === "#success" || window.location.hash === '#success-simple-form') {
    history.pushState(null, null, '/');
    document.getElementById("success-msg").style = "display: block";
    setTimeout(() => {
      document.getElementById("success-msg").style = "display: none";
    }, 4000);
  }

  if (window.location.hash === "#error" || window.location.hash === '#success-simple-form') {
    history.pushState(null, null, '/');
    document.getElementById("error-msg").style = "display: block";
    setTimeout(() => {
      document.getElementById("error-msg").style = "display: none";
    }, 4000);
  }

  window.addEventListener("click", function (e) {
    const id = event.target.id;
    if (id === "Capa_1" || id === "Capa_2" || id === "Capa_3" || id === "Capa_4") { // клик на info
      const display = document.getElementById("about").style.display;
      if (display === 'none') {
        document.getElementById("about").style = "display: block";
      } else {
        document.getElementById("about").style = "display: none";
      }
    }
  })
};
